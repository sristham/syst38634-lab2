package time;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimeTest {

	@Test
	public void testGetTotalMilliseconds() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:05:05");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 5);
	}
	
	@Test(expected=NumberFormatException.class)
	public void testGetTotalMillisecondsException() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:05:0a");
		fail("The milliseconds provied is not valid");
	}
	
	@Test
	public void testGetTotalMillisecondsBoundryIn() {
		int totalMilliseconds = Time.getTotalMilliseconds("00:05:05:59");
		assertTrue("The milliseonds does not match the result", totalMilliseconds == 59);
	}
	
	@Test(expected=NumberFormatException.class)
	public void testGetTotalMillisecondsBoundryOut() {
		int totalMilliseconds = Time.getTotalMilliseconds("100:05:59:59");
		fail("The milliseconds provied is not valid");
	}
	
	@Test
	public void testGetTotalSeconds() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		assertTrue("The time provided does not match the result", totalSeconds == 3661);
	}
	
	@Test(expected=NumberFormatException.class)
	public void testGetTotalSecondsException() {
		int totalSeconds = Time.getTotalSeconds("01:01:0a");
		fail("The time provied is not valid");
	}
	
	@Test
	public void testGetTotalSecondsBoundryIn() {
		int totalSeconds = Time.getTotalSeconds("00:00:59");
		assertTrue("The time provided does not match the result", totalSeconds == 59);
	}
	
	@Test(expected=NumberFormatException.class)
	public void testGetTotalSecondsBoundryOut() {
		int totalSeconds = Time.getTotalSeconds("01:01:60");
		fail("The time provied is not valid");
	}
}
